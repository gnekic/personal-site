import React from 'react';
import Link from 'gatsby-link';
import get from 'lodash/get';
import Helmet from 'react-helmet';
import styled from 'styled-components';
import { backgroundAlt, headingColour, textColour, mainColour } from '../variables';

import Hero from '../components/Hero';
import BlogPrompt from '../components/BlogPrompt';
import Container from '../components/Container';
import Work from '../components/Work';
import OtherWork from '../components/OtherWork';

const WorkWrapper = styled.div`
  padding: 75px 0 45px;
`;

const OtherWrapper = styled.div`
  background: ${backgroundAlt};
  padding: 75px 0 45px;
`;

const SectionHeader = styled.h2`
  color: ${headingColour};
  margin: 0;
  text-align: center;
`;

const SectionDescription = styled.p`
  color: ${textColour};
  margin: 0 0 75px;
  text-align: center;

  a {
    color: ${mainColour};
    text-decoration: none;
  }
`;

const OtherWorkWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

class BlogIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title');
    const posts = get(this, 'props.data.allMarkdownRemark.edges');

    return (
      <div>
        <Helmet title={get(this, 'props.data.site.siteMetadata.title')} />
        <Hero/>
        <WorkWrapper id="projects">
          <Container>
            <SectionHeader>Projects</SectionHeader>
            <SectionDescription>Browse through some of the larger projects I've worked on</SectionDescription>
            {posts.map(post => {
              if (post.node.path !== '/404/') {
                return (
                  <Work 
                    key={post.node.frontmatter.path}
                    title={post.node.frontmatter.title}
                    type={post.node.frontmatter.type}
                    link={post.node.frontmatter.path}
                    image={post.node.frontmatter.thumbnail.childImageSharp.responsiveSizes.src}
                  />
                )
              }
            })}
          </Container>
        </WorkWrapper>
        <OtherWrapper>
          <Container>
            <SectionHeader>Other Work</SectionHeader>
            <SectionDescription>To see a complete list, visit my GitHub and Bitbucket</SectionDescription>
            <OtherWorkWrapper>
              <OtherWork name="GeXxJs" description="GeXxJS Javascript dialect. Idea is to transform any Javascript code into valid Javascript code based of only 8 possible chars." tags={["JavaScript"]} link="https://github.com/Gexx/GeXxJS" />
              <OtherWork name="Chat" description="Socket.io chat created for Kamp Informatičara Hrvatske 2018." tags={["Node.js", "Socket.io", "JavaScript"]} link="https://bitbucket.org/gnekic/radionica-nodejs-chat-kamp2018" />
              <OtherWork name="Demo Bot for Bot Fight Arena" description="A example bot that plays the game." tags={["Node.js", "Socket.io", "JavaScript"]} link="https://bitbucket.org/gnekic/bot-example-for-bot-fight-arena" />
              <OtherWork name="Bot Fight Arena" description="Bot fight arena, a game created for Kamp Informatičara Hrvatske 2018." tags={["Node.js", "Socket.io", "JavaScript"]} link="https://bitbucket.org/gnekic/bot-fight-arena" />
              <OtherWork name="Personal Blog" description="My personal blog built in Gatsby and React" tags={["React", "GatsbyJS"]} link="https://bitbucket.org/gnekic/personal-blog" />
              <OtherWork name="Personal Site" description="My personal site (this one!) built in Gatsby and React" tags={["React", "GatsbyJS"]} link="https://bitbucket.org/gnekic/personal-site" />
            </OtherWorkWrapper>
          </Container>
        </OtherWrapper>
        <BlogPrompt/>
      </div>
    )
  }
}

BlogIndex.propTypes = {
  route: React.PropTypes.object,
};

export default BlogIndex;

export const pageQuery = graphql`
  query IndexQuery {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      filter: { frontmatter: { published: { eq:"true" } } },
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          frontmatter {
            title
            path
            type
            date(formatString: "DD MMMM, YYYY")
            thumbnail {
              childImageSharp {
                responsiveSizes(maxWidth: 600) {
                  src
                }
              }
            }
          }
        }
      }
    }
  }
`;
