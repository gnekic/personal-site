---
title: "BonusDrivePlus"
type: "Web App"
date: "2018-08-01"
path: "/bonusdriveplus/"
thumbnail: "./thumbnail.jpg"
published: "false"
---

## Overview

While working for iOLAP Inc. as a member of ADST team I was working on BonusDrive project.

Stack we used:

Node.js, Express.js, GraphQl, CosmosDb (Backend)

React, Redux, Redux Saga (Frontend)

## Result

Below you can see some of screenshots from the site.

![Screenshot 1](./image1.jpg)
