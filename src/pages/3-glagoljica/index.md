---
title: "Glagoljica Transpiler"
type: "Web Page"
date: "2018-08-01"
path: "/glagoljica/"
thumbnail: "./thumbnail.jpg"
published: "true"
---

## Overview

While reading about Unicode blocks I stumbled upon Glagolitic Unicode block [Wiki](https://en.wikipedia.org/wiki/Glagolitic_(Unicode_block) "Wiki")

And well, the rest is history.

To see it live, [click here](https://neki.ch/glagoljica "Glagoljica Transpiler").

## Misc

Oh yea did I mention that glagolitic writings can be found all over Witcher 3 ? :D
![Witcher 3](./witcher_3_face-758x474.jpg)

## Result

![Screenshot 1](./image1.png)

