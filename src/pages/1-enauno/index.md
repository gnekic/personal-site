---
title: "Ena.Uno Game"
type: "Web Page"
date: "2017-03-07"
path: "/enauno/"
thumbnail: "./thumbnail.jpg"
published: "true"
---

## Overview

In my free time I've created a game somewhat similar to the popular game UNO, it has different rules though.
For example if you use card '0' you have to swap cards with anyone on the table, nevertheless if it is your last card you win. There is also a way to skip the queue by using identical (same value and color) card before other players and thus playing before them.  

In addition, all graphic are custom made and the whole game is open source.

To see it live, [click here](https://ena.uno/ "Ena Uno Game").

## Result

Below you can find a number of screenshots from the game.

![Screenshot 1](./2017-03-07_120502.png)
![Screenshot 2](./2017-03-07_010310.png)
![Screenshot 3](./2017-03-07_115940.png)
![Screenshot 4](./2017-03-07_115719.png)
