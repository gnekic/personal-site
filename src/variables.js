export const mainColour = '#091440';// '#3498db';
export const mainColourHover = '#1d6fa5';
export const backgroundAlt = '#f2f2f2';// '#f5f5f5';
export const borderColour = '#e6e6e6';
export const headingColour = '#091440';
export const textColour = '#333333';// '#525f7f';
export const smallScreen = '768px';